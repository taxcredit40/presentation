<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            ['name' => 'Giallo'], //1
            ['name' => 'Fantascienza'], //2
            ['name' => 'Fantasy'], //3
            ['name' => 'Horror'], //4
        ]);
    }
}
