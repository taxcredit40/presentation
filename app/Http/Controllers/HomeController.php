<?php

namespace App\Http\Controllers;

use App\Models\Bookstore;
use App\Models\City;
use App\Models\Genre;

class HomeController extends Controller
{   
    public function home()
    {   
        return redirect()->route('bookstores.index');
    }
    
    public function index()
    {   
        $data = [
            'indexedBookstores' => [] /* Compilare */,
            'cities' => City::get()->keyBy('id'),
            'genres' => Genre::get()->keyBy('id')
        ];
        
        return view('home')->with($data);
    }
    
    public function books(Bookstore $bookstore, Genre $genre = null)
    {   
        $data = [
            'books' => [] /* Compilare */
        ];
        
        return $data;
    }
    
    public function bookstores(Genre $genre = null)
    {   
        $data = [
            'indexedBookstores' => [] /* Compilare */
        ];
        
        return $data;
    }
}
