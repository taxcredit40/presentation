<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            ['name' => 'Genova'], //1
            ['name' => 'Savona'], //2
            ['name' => 'Imperia'], //3
            ['name' => 'Rapallo'], //4
            ['name' => 'Chiavari'], //5
        ]);
    }
}
