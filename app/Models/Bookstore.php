<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookstore extends Model
{
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
    
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    
    public static function filter($genre = null)
    {           
        return self::with(['books' => function($query) use ($genre){
            if($genre){
                $query->where('genre_id', $genre->id);
            }
        }])->get()->groupBy('city_id');
    }
    
    public function filterBooks($genre = null){
        $booksQuery = $this->books();
        
        if($genre){
            $booksQuery = $booksQuery->where('genre_id', $genre->id);
        }
        
        return $booksQuery->get();
    }
}
