<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home');
Route::get('/bookstores', [App\Http\Controllers\HomeController::class, 'index'])->name('bookstores.index');
Route::get('/bookstores/{bookstore}/books/{genre?}', [App\Http\Controllers\HomeController::class, 'books'])->name('bookstores.books.index');
Route::get('/bookstores/books/{genre?}', [App\Http\Controllers\HomeController::class, 'bookstores'])->name('bookstores.filter');
