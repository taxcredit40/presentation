<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookstoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bookstores')->insert([
            ['name' => 'Libraccio', 'address' => 'Piazza Rossetti 56', 'city_id' => '1'], //1
            ['name' => 'Feltrinelli', 'address' => 'Via XX settembre 5', 'city_id' => '1'], //2
            ['name' => 'Mondadori', 'address' => 'Piazza Dante 86', 'city_id' => '1'], //3
            
            ['name' => 'Piccolo libro', 'address' => 'Via Piave 4', 'city_id' => '2'], //4
            
            ['name' => 'Libreria mondo', 'address' => 'Via dei pescatori 7', 'city_id' => '3'], //5
            ['name' => 'Da Dante', 'address' => 'Piazza gemelli 2', 'city_id' => '3'], //6
            
            ['name' => 'Il poeta', 'address' => 'Via Lazzaro 4', 'city_id' => '4'], //7
            ['name' => 'Libraccio', 'address' => 'Piazza Merani 1/b', 'city_id' => '4'], //8
            ['name' => 'Mondadori', 'address' => 'Piazza Merani 23', 'city_id' => '4'], //9
            
            ['name' => 'Libreria Chiavari', 'address' => 'Via Roma 2', 'city_id' => '5'], //10
            ['name' => 'Libraccio', 'address' => 'Via Venezia 16', 'city_id' => '5'], //11
        ]);
    }
}
