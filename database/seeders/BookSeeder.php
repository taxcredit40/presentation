<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            ['name' => "Assassinio sull'Orient Express", 'genre_id' => 1], //1
            ['name' => "Il mastino dei Baskerville", 'genre_id' => 1], //2
            ['name' => "Uno studio in rosso", 'genre_id' => 1], //3
            ['name' => "I delitti della Rue Morgue", 'genre_id' => 1], //4
            ['name' => "Arsène Lupin, ladro gentiluomo", 'genre_id' => 1], //5
            
            ['name' => "Farenheit 451", 'genre_id' => 2], //6
            ['name' => "1984", 'genre_id' => 2], //7
            ['name' => "Neuromante", 'genre_id' => 2], //8
            ['name' => "Io, Robot", 'genre_id' => 2], //9
            
            ['name' => "Lo Hobbit", 'genre_id' => 3], //10
            ['name' => "Il signore degli anelli", 'genre_id' => 3], //11
            ['name' => "Game of Thrones", 'genre_id' => 3], //12
            ['name' => "Harry Potter", 'genre_id' => 3], //13
            ['name' => "The Witcher", 'id' => 3], //14
            
            ['name' => "IT", 'id' => 4], //15
            ['name' => "Dracula", 'id' => 4], //15
            ['name' => "Frankenstein", 'id' => 4], //15
            ['name' => "Shining", 'id' => 4], //15
        ]);
        
        DB::table('book_bookstore')->insert([
            ['book_id' => 1, 'bookstore_id' => 1],
            ['book_id' => 1, 'bookstore_id' => 2],
            ['book_id' => 1, 'bookstore_id' => 5],
            ['book_id' => 1, 'bookstore_id' => 10],
            ['book_id' => 1, 'bookstore_id' => 11],
            
            ['book_id' => 2, 'bookstore_id' => 2],
            ['book_id' => 2, 'bookstore_id' => 3],
            ['book_id' => 2, 'bookstore_id' => 8],
            ['book_id' => 2, 'bookstore_id' => 9],
            ['book_id' => 2, 'bookstore_id' => 11],
            
            ['book_id' => 3, 'bookstore_id' => 5],
            ['book_id' => 3, 'bookstore_id' => 6],
            ['book_id' => 3, 'bookstore_id' => 7],
            ['book_id' => 3, 'bookstore_id' => 8],
            ['book_id' => 3, 'bookstore_id' => 9],
            
            ['book_id' => 4, 'bookstore_id' => 1],
            ['book_id' => 4, 'bookstore_id' => 2],
            ['book_id' => 4, 'bookstore_id' => 3],
            ['book_id' => 4, 'bookstore_id' => 7],
            ['book_id' => 4, 'bookstore_id' => 8],
            ['book_id' => 4, 'bookstore_id' => 10],
            
            ['book_id' => 5, 'bookstore_id' => 2],
            ['book_id' => 5, 'bookstore_id' => 3],
            ['book_id' => 5, 'bookstore_id' => 7],
            ['book_id' => 5, 'bookstore_id' => 8],
            ['book_id' => 5, 'bookstore_id' => 9],
            ['book_id' => 5, 'bookstore_id' => 10],
            
            ['book_id' => 6, 'bookstore_id' => 4],
            ['book_id' => 6, 'bookstore_id' => 8],
            ['book_id' => 6, 'bookstore_id' => 11],
            
            ['book_id' => 7, 'bookstore_id' => 1],
            ['book_id' => 7, 'bookstore_id' => 2],
            ['book_id' => 7, 'bookstore_id' => 5],
            ['book_id' => 7, 'bookstore_id' => 8],
            
            ['book_id' => 8, 'bookstore_id' => 1],
            ['book_id' => 8, 'bookstore_id' => 7],
            
            ['book_id' => 9, 'bookstore_id' => 4],
            ['book_id' => 9, 'bookstore_id' => 8],
            ['book_id' => 9, 'bookstore_id' => 10],
            
            ['book_id' => 10, 'bookstore_id' => 1],
            ['book_id' => 10, 'bookstore_id' => 2],
            ['book_id' => 10, 'bookstore_id' => 3],
            ['book_id' => 10, 'bookstore_id' => 4],
            ['book_id' => 10, 'bookstore_id' => 7],
            ['book_id' => 10, 'bookstore_id' => 8],
            ['book_id' => 10, 'bookstore_id' => 11],
            
            ['book_id' => 11, 'bookstore_id' => 1],
            ['book_id' => 11, 'bookstore_id' => 4],
            ['book_id' => 11, 'bookstore_id' => 8],
            ['book_id' => 11, 'bookstore_id' => 10],
            
            ['book_id' => 12, 'bookstore_id' => 4],
            ['book_id' => 12, 'bookstore_id' => 7],
            ['book_id' => 12, 'bookstore_id' => 8],
            
            ['book_id' => 13, 'bookstore_id' => 1],
            ['book_id' => 13, 'bookstore_id' => 5],
            ['book_id' => 13, 'bookstore_id' => 6],
            ['book_id' => 13, 'bookstore_id' => 8],
            
            ['book_id' => 14, 'bookstore_id' => 1],
            ['book_id' => 14, 'bookstore_id' => 3],
            ['book_id' => 14, 'bookstore_id' => 4],
            
            ['book_id' => 15, 'bookstore_id' => 2],
            ['book_id' => 15, 'bookstore_id' => 3],
            ['book_id' => 15, 'bookstore_id' => 4],
            ['book_id' => 15, 'bookstore_id' => 7],
            ['book_id' => 15, 'bookstore_id' => 9],
            ['book_id' => 15, 'bookstore_id' => 11],
        ]);
    }
}